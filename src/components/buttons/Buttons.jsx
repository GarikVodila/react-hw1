import './Buttons.module.scss'


function Button (props){

const style = {
    background:props.backgroundcolor,
}  


    return(
        <button 
        className='btn' 
        onClick={props.onClick} 
        style={style}
        >{props.text}</button>
    )
}

export default Button