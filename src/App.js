import { useState } from 'react';
import Button from './components/buttons/Buttons';
import Modal from './components/modals/Modal';


function App() {
// відкриття FirstModal
  const [openFirstModal, setOpenFirstModal] = useState(false)
  const FirstClick = () =>{
    setOpenFirstModal(true)
  }
// закриття FirstModal
  const CloseFirstModal = ()=>{
    setOpenFirstModal(false)
  }

// відкриття SecondModal
  const [openSecondModal, setOpenSecondModal] = useState(false)
  const SecondClick = () =>{
    setOpenSecondModal(true)
  }
  const CloseSecondModal = ()=>{
    setOpenSecondModal(false)
  }

  // Закриття усіх Modals

  return (
    <>
      <div>
        <Button 
          onClick ={FirstClick} 
          backgroundcolor = "crimson" 
          text = "Open first modal"
        />
        <Button 
          onClick = {SecondClick}
          backgroundcolor="yellowgreen" 
          text = "Open second modal"
        />
      </div>


      { openFirstModal && <Modal className="block1"
        backgroundcolor='lightcoral'
        header="Заголовок першого блоку"
        text = "text of first modal"
        buttonFirstText="OK"
        buttonSecondText="Cansel"
        actions={CloseFirstModal}
      /> } 
      { openSecondModal && <Modal className="block2"
        backgroundcolor='lightblue'
        header="Заголовок другого блоку"
        text = "text of second modal"
        buttonFirstText="OK"
        buttonSecondText="Cansel"
        actions={CloseSecondModal}
      />}

    </>

);
}

export default App;
